package com.sam.ui.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.WindowManager;
import android.widget.NumberPicker;

import com.sam.R;
import com.sam.model.CarInfo;
import com.sam.utils.Utils;

/**
 * @author eugenii.samarskyi.
 */
public class RateDialog implements DialogInterface.OnClickListener {

	public interface Callback {
		void onEdited(int rate, CarInfo carInfo);
	}

	private Context mContext;

	private NumberPicker mNumberPicker;

	private Callback mCallback;
	private CarInfo mCarInfo;

	public RateDialog(Context context, Callback callback, CarInfo carInfo) {
		mContext = context;
		mCallback = callback;
		mCarInfo = carInfo;

		mNumberPicker = Utils.inflate(mContext, R.layout.v_number_picker);
		mNumberPicker.setMinValue(1);
		mNumberPicker.setMaxValue(10);
		mNumberPicker.setValue(1);
	}

	public void show() {
		new AlertDialog.Builder(mContext)
				.setTitle(R.string.rate_title)
				.setView(mNumberPicker)
				.setPositiveButton(R.string.ok, this)
				.setNegativeButton(R.string.cancel, null)
				.show()
				.getWindow().clearFlags(
					WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		if (mCallback != null) {
			mCallback.onEdited(mNumberPicker.getValue(), mCarInfo);
		}
	}
}
