package com.sam.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sam.R;
import com.sam.ui.base.BaseFragment;

/**
 * @author eugenii.samarskyi.
 */
public class MainFragment extends BaseFragment implements View.OnClickListener {

	public static MainFragment newInstance() {

		Bundle args = new Bundle();

		MainFragment fragment = new MainFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.f_main, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		view.findViewById(R.id.photo).setOnClickListener(this);
		view.findViewById(R.id.share).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		Fragment fragment = null;
		switch (v.getId()) {
			case R.id.photo:
				fragment = PhotoListFragment.newInstance();
				break;
			case R.id.share:
				fragment = BrandFragment.newInstance();
				break;
		}

		if (fragment == null) {
			return;
		}

		getActivity().getSupportFragmentManager().beginTransaction()
				.setCustomAnimations(
						R.anim.enter_from_right, R.anim.exit_to_left,
						R.anim.enter_from_left, R.anim.exit_to_right
				)
				.replace(R.id.content, fragment)
				.addToBackStack(null)
				.commit();
	}
}
