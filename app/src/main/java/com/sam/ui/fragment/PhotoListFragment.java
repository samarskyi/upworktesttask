package com.sam.ui.fragment;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.sam.R;
import com.sam.model.CarInfo;
import com.sam.ui.base.BaseFragment;
import com.sam.ui.dialog.RateDialog;
import com.sam.utils.FirebaseImageLoader;
import com.sam.utils.ItemDiffCallback;
import com.sam.utils.Utils;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author eugenii.samarskyi.
 */
public class PhotoListFragment extends BaseFragment implements RateDialog.Callback, ValueEventListener {

	private static final String TAG = PhotoListFragment.class.getSimpleName();

	private final GenericTypeIndicator<List<CarInfo>> mapType = new GenericTypeIndicator<List<CarInfo>>() {
	};

	private RecyclerView mListView;

	private List<CarInfo> mData;
	private StorageReference mStorageRef;
	private DatabaseReference myDbRef;

	private View mProgressBar;
	private Drawable mCurrentShare;

	private static final int PERMISSIONS_REQUEST = 101;

	public static Fragment newInstance() {
		return new PhotoListFragment();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.f_list, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		if (!Utils.isNetworkConnected()) {
			return;
		}

		mProgressBar = view.findViewById(R.id.progress);

		mListView = (RecyclerView) view.findViewById(R.id.list);
		mListView.setLayoutManager(new LinearLayoutManager(getActivity()));

		mStorageRef = FirebaseStorage.getInstance().getReferenceFromUrl("gs://upworktesttask.appspot.com/images");
		myDbRef = FirebaseDatabase.getInstance().getReference("message");

		myDbRef.addValueEventListener(this);
	}

	@Override
	public void onEdited(int rate, CarInfo carInfo) {
		if (carInfo.getRatingValues() == null) {
			carInfo.setRatingValues(new ArrayList<Integer>());
		}
		carInfo.getRatingValues().add(rate);

		myDbRef.setValue(mData);
	}

	@Override
	public void onDataChange(DataSnapshot dataSnapshot) {
		if (mProgressBar.getVisibility() == View.VISIBLE) {
			mProgressBar.setVisibility(View.GONE);
		}
		if (mData == null) {
			mData = dataSnapshot.getValue(mapType);
			mListView.setAdapter(new Adapter());
		} else {
			DiffUtil.DiffResult result = DiffUtil.calculateDiff(new ItemDiffCallback(mData, dataSnapshot.getValue(mapType)));
			result.dispatchUpdatesTo(mListView.getAdapter());
		}
	}

	@Override
	public void onCancelled(DatabaseError databaseError) {
		Log.e(TAG, "onCancelled: " + databaseError.getDetails());
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
		switch (requestCode) {
			case PERMISSIONS_REQUEST: {
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					onPermissionGranted(mCurrentShare);
				}
			}
		}
	}

	private class Adapter extends RecyclerView.Adapter<Holder> implements View.OnClickListener {

		@Override
		public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
			return new Holder(Utils.inflate(parent, R.layout.v_car_item));
		}

		@Override
		public void onBindViewHolder(Holder holder, int position) {
			CarInfo carInfo = mData.get(position);

			StorageReference pathReference = mStorageRef.child(carInfo.getFileName());
			holder.ratingView.setText(carInfo.getAverage());

			Glide.with(getActivity())
					.using(new FirebaseImageLoader())
					.load(pathReference)
					.override(mListView.getWidth(), (int) getResources().getDimension(R.dimen.image_h))
					.centerCrop()
					.into(holder.imageView);

			holder.rateView.setTag(carInfo);
			holder.rateView.setOnClickListener(this);

			holder.shareView.setTag(holder.imageView);
			holder.shareView.setOnClickListener(this);
		}

		@Override
		public void onBindViewHolder(Holder holder, int position, List<Object> payloads) {
			if (payloads.isEmpty()) {
				onBindViewHolder(holder, position);
			} else {
				CarInfo carInfo = (CarInfo) payloads.get(0);
				holder.ratingView.setText((carInfo.getAverage()));

				for (CarInfo info : mData) {
					if (info.getFileName().equals(carInfo.getFileName())) {
						info.setRatingValues(carInfo.getRatingValues());
					}
				}
			}
		}

		@Override
		public int getItemCount() {
			return mData.size();
		}

		@Override
		public void onClick(View v) {
			if (R.id.rate == v.getId()) {
				new RateDialog(getActivity(), PhotoListFragment.this, (CarInfo) v.getTag()).show();
			} else {
				mCurrentShare = ((ImageView) v.getTag()).getDrawable();

				if (ContextCompat.checkSelfPermission(getActivity(),
						Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
					ActivityCompat.requestPermissions(getActivity(),
							new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSIONS_REQUEST);
				} else {
					onPermissionGranted(mCurrentShare);
				}
			}
		}
	}

	class Holder extends RecyclerView.ViewHolder {
		final ImageView imageView;
		final TextView ratingView;
		final ImageView rateView;
		final View shareView;

		Holder(View itemView) {
			super(itemView);

			rateView = (ImageView) itemView.findViewById(R.id.rate);
			imageView = (ImageView) itemView.findViewById(R.id.image);
			ratingView = (TextView) itemView.findViewById(R.id.rating);
			shareView = itemView.findViewById(R.id.share);
		}
	}

	private void onPermissionGranted(Drawable drawable) {
		Bitmap icon = drawableToBitmap(drawable);
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("image/jpeg");

		ContentValues values = new ContentValues();
		values.put(MediaStore.Images.Media.TITLE, "title");
		values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
		Uri uri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
		if (uri == null) {
			return;
		}

		try {
			OutputStream outstream = getActivity().getContentResolver().openOutputStream(uri);
			icon.compress(Bitmap.CompressFormat.JPEG, 100, outstream);
			if (outstream != null) {
				outstream.close();
			}
		} catch (Exception e) {
			Log.e(TAG, "onPermissionGranted: ", e);
		}

		intent.putExtra(Intent.EXTRA_STREAM, uri);
		startActivity(Intent.createChooser(intent, "Share Image"));
	}

	private Bitmap drawableToBitmap(Drawable drawable) {
		Bitmap bitmap = null;

		if (drawable instanceof BitmapDrawable) {
			BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
			if (bitmapDrawable.getBitmap() != null) {
				return bitmapDrawable.getBitmap();
			}
		}

		if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
			bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
		} else {
			bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
		}

		Canvas canvas = new Canvas(bitmap);
		drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
		drawable.draw(canvas);

		return bitmap;
	}
}
