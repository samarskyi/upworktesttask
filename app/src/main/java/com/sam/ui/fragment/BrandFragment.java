package com.sam.ui.fragment;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sam.R;
import com.sam.model.BrandInfo;
import com.sam.ui.base.BaseFragment;
import com.sam.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author eugenii.samarskyi.
 */
public class BrandFragment extends BaseFragment {

	public static Fragment newInstance() {
		return new BrandFragment();
	}

	private List<BrandInfo> mInfoList;

	private RecyclerView mListView;

	private SparseBooleanArray expandData = new SparseBooleanArray();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.f_brands, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		mInfoList = generateInfo();
		for (int i = 0; i < mInfoList.size(); i++) {
			expandData.put(i, false);
		}

		mListView = (RecyclerView) view.findViewById(R.id.list);
		mListView.setLayoutManager(new LinearLayoutManager(getActivity()));

		mListView.setAdapter(new Adapter());
	}

	private List<BrandInfo> generateInfo() {
		List<BrandInfo> list = new ArrayList<>();

		list.add(new BrandInfo("678 456 123", "673test@gmail.com", "WindowsCallCenter ready", "https://www.facebook.com/WindowsPhoneCentral"));
		list.add(new BrandInfo("123 456 78", "123456test@gmail.com", "iPhone Test Sample text", "https://www.facebook.com/supportapples"));
		list.add(new BrandInfo("12 56 67 78", "1236test@gmail.com", "Blackberry modern tes task", "https://www.facebook.com/BlackBerry"));
		list.add(new BrandInfo("456 678 99", "1fast@gmail.com", "Enjoy working with Android", "https://www.facebook.com/AndroidOfficial"));
		list.add(new BrandInfo("123 4544 678", "gofirst@gmail.com", "Browser make it easiery", "https://www.facebook.com/AndroidOfficial"));

		return list;
	}

	class Adapter extends RecyclerView.Adapter<Holder> implements View.OnClickListener {

		@Override
		public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
			return new Holder(Utils.inflate(parent, R.layout.v_brand_item));
		}

		@Override
		public void onBindViewHolder(Holder holder, int position) {
			BrandInfo info = mInfoList.get(position);
			boolean isExpanded = expandData.get(position);

			holder.name.setText(info.getText());
			holder.expand.setImageResource(isExpanded ? R.drawable.ic_expand_more : R.drawable.ic_chevron_right);
			holder.expanded.setVisibility(isExpanded ? View.VISIBLE : View.GONE);

			if (isExpanded) {
				holder.emailView.setText(info.getEmail());
				holder.emailView.setTag(info.getEmail());
				holder.emailView.setOnClickListener(this);

				holder.phoneView.setText(info.getPhoneNumber());
				holder.phoneView.setTag(info.getPhoneNumber());
				holder.phoneView.setOnClickListener(this);

				holder.shareView.setTag(info.getShare());
				holder.shareView.setOnClickListener(this);

				holder.infoView.setText(info.getText());
			}

			holder.expand.setTag(mInfoList.get(position));
			holder.expand.setOnClickListener(this);
		}

		@Override
		public int getItemCount() {
			return mInfoList.size();
		}

		@Override
		public void onClick(View v) {
			if (R.id.phone == v.getId()) {
				Intent intent = new Intent(Intent.ACTION_DIAL);
				intent.setData(Uri.parse("tel:" + v.getTag()));
				startActivity(intent);
			} else if (R.id.email == v.getId()) {
				Intent intent = new Intent(Intent.ACTION_SEND);
				intent.setType("plain/text");
				intent.putExtra(Intent.EXTRA_EMAIL, new String[]{(String) v.getTag()});
				startActivity(Intent.createChooser(intent, ""));
			} else if (R.id.share == v.getId()) {
				String urlToShare = String.valueOf(v.getTag());
				Intent intent = new Intent(Intent.ACTION_SEND);
				intent.setType("text/plain");
				intent.putExtra(Intent.EXTRA_TEXT, urlToShare);

				boolean facebookAppFound = false;
				List<ResolveInfo> matches = getActivity().getPackageManager().queryIntentActivities(intent, 0);
				for (ResolveInfo info : matches) {
					if (info.activityInfo.packageName.toLowerCase().startsWith("com.facebook.katana")) {
						intent.setPackage(info.activityInfo.packageName);
						facebookAppFound = true;
						break;
					}
				}

				if (!facebookAppFound) {
					String sharerUrl = "https://www.facebook.com/sharer/sharer.php?u=" + urlToShare;
					intent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl));
				}

				startActivity(intent);
			} else {
				BrandInfo brandInfo = (BrandInfo) v.getTag();
				int position = mInfoList.indexOf(brandInfo);
				boolean isExpanded = expandData.get(position);
				expandData.put(position, !isExpanded);

				notifyItemChanged(position);
			}
		}
	}

	class Holder extends RecyclerView.ViewHolder {

		final TextView name;
		final ImageView expand;
		final ViewGroup expanded;

		final TextView phoneView;
		final TextView emailView;
		final TextView infoView;
		final ImageView shareView;

		Holder(View itemView) {
			super(itemView);

			name = (TextView) itemView.findViewById(R.id.name);
			expand = (ImageView) itemView.findViewById(R.id.expand);
			expanded = (ViewGroup) itemView.findViewById(R.id.expanded);

			infoView = (TextView) itemView.findViewById(R.id.info);
			phoneView = (TextView) itemView.findViewById(R.id.phone);
			emailView = (TextView) itemView.findViewById(R.id.email);
			shareView = (ImageView) itemView.findViewById(R.id.share);
		}
	}
}
