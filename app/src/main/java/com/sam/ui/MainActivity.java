package com.sam.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.sam.R;
import com.sam.ui.fragment.MainFragment;

/**
 * @author eugenii.samarskyi.
 */
public class MainActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.a_main);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.content, MainFragment.newInstance())
					.commit();
		}
	}
}
