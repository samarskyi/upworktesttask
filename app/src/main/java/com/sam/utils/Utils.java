package com.sam.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sam.App;

/**
 * @author eugenii.samarskyi.
 */
public class Utils {

	@SuppressWarnings("unchecked")
	public static <T extends View> T inflate(ViewGroup parent, int resourceId) {
		return (T) LayoutInflater.from(parent.getContext()).inflate(resourceId, parent, false);
	}

	@SuppressWarnings("unchecked")
	public static <T extends View> T inflate(Context context, int resourceId) {
		return (T) LayoutInflater.from(context).inflate(resourceId, null, false);
	}

	public static boolean isNetworkConnected() {
		ConnectivityManager cm = (ConnectivityManager) App.self().getSystemService(Context.CONNECTIVITY_SERVICE);

		return cm.getActiveNetworkInfo() != null;
	}
}
