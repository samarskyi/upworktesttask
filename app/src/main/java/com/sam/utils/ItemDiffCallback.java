package com.sam.utils;

import android.support.v7.util.DiffUtil;

import com.sam.model.CarInfo;

import java.util.List;

/**
 * @author eugenii.samarskyi.
 */
public class ItemDiffCallback extends DiffUtil.Callback {

	private List<CarInfo> oldList;
	private List<CarInfo> newList;

	public ItemDiffCallback(List<CarInfo> oldList, List<CarInfo> newList) {
		this.oldList = oldList;
		this.newList = newList;
	}

	@Override
	public int getOldListSize() {
		return oldList == null ? 0 : oldList.size();
	}

	@Override
	public int getNewListSize() {
		return newList == null ? 0 : newList.size();
	}

	@Override
	public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
		return oldList.get(oldItemPosition).getFileName().equals(newList.get(newItemPosition).getFileName());
	}

	@Override
	public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
		return newList.get(newItemPosition).equals(oldList.get(oldItemPosition));
	}

	@Override
	public Object getChangePayload(int oldPosition, int newPosition) {
		return newList.get(newPosition);
	}
}
