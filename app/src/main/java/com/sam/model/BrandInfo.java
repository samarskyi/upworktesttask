package com.sam.model;

/**
 * @author eugenii.samarskyi.
 */
public class BrandInfo {
	private String text;
	private String email;
	private String share;
	private String phoneNumber;

	public BrandInfo(String phoneNumber, String email, String text, String share) {
		this.text = text;
		this.email = email;
		this.share = share;
		this.phoneNumber = phoneNumber;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getShare() {
		return share;
	}

	public void setShare(String share) {
		this.share = share;
	}
}
