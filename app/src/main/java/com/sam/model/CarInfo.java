package com.sam.model;

import java.text.DecimalFormat;
import java.util.List;

/**
 * @author eugenii.samarskyi.
 */
public class CarInfo {
	private static final DecimalFormat df = new DecimalFormat("#.00");

	private String fileName;
	private List<Integer> ratingValues;

	public CarInfo() {
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public List<Integer> getRatingValues() {
		return ratingValues;
	}

	public void setRatingValues(List<Integer> ratingValues) {
		this.ratingValues = ratingValues;
	}

	public String getAverage() {
		Integer sum = 0;
		if (ratingValues == null || ratingValues.isEmpty()) {
			return df.format(sum);
		}

		for (Integer mark : ratingValues) {
			if (mark != null) {
				sum += mark;
			}
		}

		return df.format(sum.doubleValue() / ratingValues.size());
	}
}
