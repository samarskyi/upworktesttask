package com.sam;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author eugenii.samarskyi.
 */
public class App extends Application {

	private static App self;

	private Gson mGson = new GsonBuilder().serializeNulls().create();

	@Override
	public void onCreate() {
		super.onCreate();

		self = this;
	}

	public static App self(){
		return self;
	}

	public static Gson getGson() {
		return self.mGson;
	}
}
